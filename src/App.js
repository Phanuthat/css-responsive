import {
  ThemeProvider,
  createTheme,
  StyledEngineProvider,
} from '@mui/material/styles';
import LandingPage from './LandingPage';
import 'swiper/swiper-bundle.min.css';
import 'swiper/swiper.min.css';
const theme = createTheme();

function App() {
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <LandingPage />
      </ThemeProvider>
    </StyledEngineProvider>
  );
}

export default App;
