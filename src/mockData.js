export const dataSectionFirst = [
  {
    title: 'ATHLETS',
    subTitle: 'CONNECTION',
    detail:
      'Connect with coaches directly, you can ping coaches to view profile.',
    bgColor: '#fff',
  },
  {
    title: '',
    subTitle: 'COLLABORATION',
    detail:
      'Work with other student athletes to increase visability. When you share and like other players videos it will increase your visability as a player. This is the team work aspect to Surface 1.',
    bgColor: '#5E3DB3',
  },
  {
    title: '',
    subTitle: 'GROWTH',
    detail:
      'Resources and tools for you to get better as a student Athelte.Access to training classes, tutor sessions, etc ',
    bgColor: '#F5F4F9',
  },
];

export const dataSectionLast = [
  {
    title: 'PLAYERS',
    subTitle: 'CONNECTION',
    detail:
      'Connect with talented athlete directly, you can watch their skills through video showreels directly from Surface 1.',
    bgColor: '#fff',
  },
  {
    title: '',
    subTitle: 'COLLABORATION',
    detail:
      'Work with recruiter to increase your chances of finding talented athlete.',
    bgColor: '#5E3DB3',
  },
  {
    title: '',
    subTitle: 'GROWTH',
    detail: 'Save your time, recruit proper athlets for your team.',
    bgColor: '#090C35',
  },
];
