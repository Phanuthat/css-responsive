import React from 'react';
import { Container, Grid, Typography } from '@mui/material';
import footballer from './footballer.svg';
import player from './player.svg';

import { makeStyles } from '@mui/styles';
import Swiper from 'react-id-swiper';
import { dataSectionFirst, dataSectionLast } from './mockData';
const params = {
  slidesPerView: 1,
  spaceBetween: 1,
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: true,
  },
  rebuildOnUpdate: true,
};
const useStyles = makeStyles(
  (theme) =>
    console.log('theme', theme) || {
      title: {
        paddingBottom: '2rem',
        paddingTop: '2rem',
      },
      container: {
        position: 'relavtive',
        display: 'inline',
      },
      img: {
        [theme.breakpoints.up(800)]: {
          maxWidth: '25rem',
          margin: '3rem',
          position: 'absolute',
          marginLeft: 'auto',
          marginRight: 'auto',
        },
        [theme.breakpoints.between(600, 800)]: {
          maxWidth: '25rem',
          margin: '3rem',
          position: 'absolute',
          marginLeft: 'auto',
          marginRight: 'auto',
        },
        [theme.breakpoints.down(600)]: {
          maxWidth: '25rem',
          margin: '3rem',
          marginLeft: 'auto',
          marginRight: 'auto',
          display: 'block',
        },
      },
      isMobile: {
        [theme.breakpoints.down(600)]: {
          display: 'none',
        },
      },
      isDesktop: {
        [theme.breakpoints.up(600)]: {
          display: 'none',
        },
      },
      sectionContainer: {
        width: '100%',
        display: 'item-list',
        direction: 'ltr',
      },
    }
);

const ListSection = ({ data = [] }) => {
  return data ? (
    data.map(({ title, subTitle, detail, bgColor }, index) => {
      return (
        <React.Fragment key={index}>
          <SectionItem
            title={title}
            subTitle={subTitle}
            detail={detail}
            bgColor={bgColor}
          />
        </React.Fragment>
      );
    })
  ) : (
    <></>
  );
};

const SectionItem = ({
  title,
  subTitle,
  detail,
  bgColor = '#fff',
  justifyContent = 'flex-end',
  alignItems = 'flex-end',
  direction = 'row',
}) => {
  return (
    <Grid
      item
      xs={12}
      md={6}
      sm={7}
      container
      direction={direction}
      alignItems={alignItems}
      justifyContent={justifyContent}
      style={{ backgroundColor: bgColor }}
    >
      <Grid item xs={12} sm={6} md={6}>
        <ItemDetail title={title} subTitle={subTitle} detail={detail} />
      </Grid>
    </Grid>
  );
};

const ItemDetail = (props) => {
  const classes = useStyles();

  return (
    <Container className={classes.title} maxWidth='md'>
      {props.title && (
        <Typography variant='h3' gutterBottom component='div'>
          {props.title}
        </Typography>
      )}
      {props.subTitle && (
        <Typography variant='h5' gutterBottom component='div'>
          {props.subTitle}
        </Typography>
      )}
      {props.detail && (
        <Typography variant='body1' gutterBottom>
          {props.detail}
        </Typography>
      )}
    </Container>
  );
};

const SwiperMobile = ({ data = [] }) => {
  return (
    <Swiper {...params}>
      {data.map(({ title, subTitle, detail, bgColor }, index) => {
        return (
          <div key={index}>
            <ItemDetail title={title} subTitle={subTitle} detail={detail} />
          </div>
        );
      })}
    </Swiper>
  );
};

export default function LandingPage() {
  const classes = useStyles();

  return (
    <Container maxWidth='xs' className={classes.container}>
      <div className={classes.sectionContainer}>
        <Container container direction='column' item xs={12}>
          <Grid item xs={12}>
            <img alt='footballer' src={footballer} className={classes.img} />
          </Grid>
        </Container>
        <Grid
          container
          direction='column'
          item
          xs={12}
          className={classes.isMobile}
        >
          <ListSection data={dataSectionFirst} />
        </Grid>
        <div
          style={{ backgroundColor: '#F5F4F9' }}
          className={classes.isDesktop}
        >
          <SwiperMobile data={dataSectionFirst} />
        </div>
      </div>
      <div className={classes.sectionContainer} style={{ direction: 'rtl' }}>
        <Container container direction='column' item xs={12}>
          <Grid item xs={12}>
            <img alt='footballer' src={footballer} className={classes.img} />
          </Grid>
        </Container>
        <Grid
          container
          direction='column'
          item
          xs={12}
          className={classes.isMobile}
        >
          <ListSection data={dataSectionLast} />
        </Grid>
        <div
          style={{ backgroundColor: '#F5F4F9' }}
          className={classes.isDesktop}
        >
          <SwiperMobile data={dataSectionLast} />
        </div>
      </div>
    </Container>
  );
}
